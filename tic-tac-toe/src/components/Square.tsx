import React, { Component } from 'react';

type MyProps = {
    value: string,
    handleSquareClick: () => void,
}

type MyState = {
    value: string,
}

class Square extends Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);

        this.state = {
            value: ""
        }
    }

    render() {
        return(
            <button className="square" onClick={this.props.handleSquareClick}>
                {this.props.value}
            </button>
        );
    }
}

export default Square;