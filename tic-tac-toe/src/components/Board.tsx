import React, { Component } from "react";

import Square from "./Square";

type MyProps = {};

type MyState = {
  squares: Array<string>;
  history: Array<Array<string>>;
  xIsNext: number;
};

type winningLines = Array<Array<number>>;

class Board extends Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);

    this.state = {
      squares: Array(9).fill(""),
      history: [Array(9).fill([""])],
      xIsNext: 1,
    };
  }

  handleClick = (i: number): void => {
    if (
      this.state.squares[i] === "" &&
      this.calculateWinner(this.state.squares) === null
    ) {
      const nextSquares = this.state.squares.slice();
      nextSquares[i] = this.state.xIsNext % 2 ? "X" : "O";
      this.setState({
        squares: nextSquares,
        history: [...this.state.history, nextSquares],
        xIsNext: this.state.xIsNext + 1,
      });
    }
  };

  calculateWinner = (squares: Array<string>): string | null => {
    const lines: winningLines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  };

  newGame = (): void => {
    this.setState({
      history: [Array(9).fill([""])],
      squares: Array(9).fill(""),
      xIsNext: 1
    });
  }

  handleHistory = (move: number): void => {
    this.setState({
      squares: this.state.history[move],
      xIsNext: move
    })
  }

  render() {
    const winner = this.calculateWinner(this.state.squares);
    let status: string = "";
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (this.state.xIsNext % 2 === 1 ? "X" : "O");
    }

    return (
      <>
        <div className="status">{status}</div>
        <div className="board">
          {this.state.squares.map((square, index) => {
            return (
              <Square
                value={square}
                handleSquareClick={() => this.handleClick(index)}
                key={index}
              />
            );
          })}
        </div>
        <div className="history">
          {this.state.history.map((board, index) => {
            if(index === 0) {
              return (
                <button key={index} onClick={this.newGame}>New Game</button>
              )
            }
            return (
              <button key={index} onClick={() => this.handleHistory(index)}>Move {index}</button>
            );
          })}
        </div>
      </>
    );
  }
}

export default Board;
